from flask import Flask
from flask_restful import Api, Resource, reqparse

from kyc import KYC
from charities import Charity 


if __name__ == '__main__':

    app = Flask(__name__)
    api = Api(app)

    api.add_resource(KYC, "/kyc/<string:ID>")
    api.add_resource(Charity, "/charities/<string:country>")
    app.run(debug=True)
