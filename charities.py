from flask import Flask
from flask_restful import Api, Resource, reqparse

charityList = [
    {
        "name":"Bill & Melinda Gates Foundation",
        "country":"USA"
    },
    {
        "name":"Wellcome Trust",
        "country":"UK"
    },
    {
        "name":"Howard Hughes Medical Institute",
        "country":"USA"
    },
    {
        "name":"Garfield Weston Foundation",
        "country":"UK"
    },
    {
        "name":"Ford Foundation",
        "country":"USA"
    },
    {
        "name":"Kamehameha Schools",
        "country":"USA"
    },
    {
        "name":"Lilly Endowment",
        "country":"USA"
    },
    {
        "name":"The Church Commissioners for England",
        "country":"UK"
    },
    {
        "name":"The MasterCard Foundation",
        "country":"Canada"
    },
    {
        "name":"Daily Bread Food Bank",
        "country":"Canada"
    },
    {
        "name":"David Foster Foundation",
        "country":"Canada"
    },
    {
        "name":"Doctors Without Borders",
        "country":"Canada"
    },
    {
        "name":"Red Cross Society of China",
        "country":"China"
    },
    {
        "name":"SOS Children's Villages",
        "country":"China"
    },
    {
        "name":"Habitat for Humanity China",
        "country":"China"
    },
    {
        "name":"Love Without Boundaries",
        "country":"China"
    }
]

class Charity(Resource):
    def get(self, country):
        result=list()
        for charity in charityList:
            if(country==charity["country"]):
                result.append(charity)
        if(len(result)==0):
            return "No charity organization found", 404
        else:
            return result, 200
