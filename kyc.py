from flask import Flask
from flask_restful import Api, Resource, reqparse

import base64
from time import sleep
from random import randint

users = list()

class KYC(Resource):

    def get(self, ID):
        for user in users:
            if user["ID"] == ID:
                return user["ID"], 200
        return "No user with ID {} has been approved".format(ID), 403

    def post(self, ID):
        parser = reqparse.RequestParser()
        parser.add_argument("image")
        args = parser.parse_args()

        for user in users:
            if (str(ID) == user["ID"]):
                return "User with ID {} has already been approved".format(ID)

        user = {"ID": ID, "image": args["image"]}
        
        if self.verify(args["image"]):
            users.append(user)
            return user, 201
        return user, 403 

    def verify(self, image):
        # Simulating KYC
        sleep(randint(2,5))
        return True

