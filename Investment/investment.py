from flask import Flask, request
from flask_restful import Resource, Api, reqparse
import time
import urllib

app = Flask(__name__)
api = Api(app)


def classicConvert(old, new, sector):
	x = max(round(new/old, 2),0.86)
	values = [y/100 for y in range(86,96)]
	if sector == 'financial':
		results = [1.0219262026975426,1.0188667077977316,1.0245552956138118,1.0085899932776043,1.0094242257342936,1.012629449673659,1.0085997039944712,1.0043325761264388,1.0046259905414183, 1.002454180032103]
	if sector == 'consumer_goods':
		results = [1.0336787245121917, 1.0018516668897035, 1.0121968136818504, 1.0243597877239592, 1.0117839118756846, 1.0072778041233288, 1.0100517598169643, 1.0043753195282119, 1.002227335160679, 0.99829828644331053, 1.0008004216501187, 1.000278724336952, 0.9999831353688966, 1.0001177470514389]
	if sector == 'industrial_goods':
		results = [1.0238691262070954, 1.0072195975214611, 1.0091992102224185, 1.0120950475554322, 1.0017874560289632, 1.0040731536728904, 1.0073926296008764, 1.0049301479719785, 1.000729026167601, 1.0004933217558043, 1.0002209347143958, 0.9999268170373502, 1.0003424524404876, 1.0002673472105459]
	if sector == 'services':
		results = [1.0061083478330159, 1.0137335518865969, 1.0071317607490105, 1.0009961387470561, 1.0051884437135918, 0.99968542065892396, 1.0021322569694517, 1.0016951508582612, 1.0019880233298362]
	if sector == 'technology':
		results = [1.0058393110528552, 1.0048492516499687, 1.0108365139080357, 1.0109392457292243, 1.0039585619019906, 1.0054884868659713, 1.0024247116105605, 1.0043359024095349, 1.0020661557181942]
	if sector == 'basic_materials':
		results = [1.0089317228327304, 0.99827798166820103, 1.0048498310306642, 0.99676885736913767, 1.0037762837995401, 0.99942960906596323, 1.0000402670852775, 0.998914269029191, 0.99801047959968014]
	return results[values.index(x)] * new	

def showCurrentPrices(sector):
	result = []
	prices = {}
	for i in range(0,25,100):
		x = urllib.urlopen("https://finance.yahoo.com/sector/" +sector+'?offset='+str(i)+'&count=100').read()
		x = x.split('"results":{"rows":')[1]
		x = x.split(']')[0]
		x = x.split('[')[1]
		print x
		y = eval(x)
		for m in y:
			prices[m['symbol']] = m['regularMarketPrice']['raw']
	return prices

def showPreviousPrices():
	file = open('financial2.txt','r').readlines()
	
	n = len(file)-2
	timestamp1 = file[n].split('\t')[0]
	timestamp2 = timestamp1
	prices = {}
	while timestamp2 == timestamp1:
		n = n-1
		line = file[n].split('\t')
		
		timestamp1 = line[0]
		try:
			prices[line[7].replace('\n','')] = float(line[1])
		except:
			pass
	return prices


class Investment(Resource):
    def get(self, number):
		result = showCurrentPrices('financial')
		resultString = ''	
		for m in result:
			resultString = resultString + m + '\t' + str(result[m]) + '\n'
		resultString = resultString + 'Jonathan recommends buying HSBC stock at $47.15 per share today.'
		return resultString
